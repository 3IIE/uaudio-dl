#!/usr/bin/env python3

import os
import subprocess as subp
from urllib.parse import urlparse
from sys import argv, stdout, exit as sExit


class youAudio():
    ''' youtube-dl -f 'bestaudio[ext=m4a]' 'http://youtu.be/hTvJoYnpeRQ' '''

    def __init__(self, *args, **krgs):
        '''  '''
        # print(f'args = {args}\nkrgs={krgs}')
        curDir = os.getcwd()

        self.url = None

        self.schms = ['http', 'https']

        if 'dest' in krgs.keys():
            if os.path.isabs(krgs['dest']) and os.path.exists(krgs['dest']):
                os.chdir(krgs['dest'])
            else:
                hPath = os.path.join(os.path.expanduser('~'), krgs['dest'])
                if not os.path.exists(hPath):
                    os.mkdir(hPath)
                    os.chdir(hPath)
                else:
                    os.chdir(hPath)

        self.getAudio(*args, **krgs)

        os.chdir(curDir)


    def getAudio(self, *args, **krgs):
        '''  '''

        if 'url' in krgs.keys():
            __url = krgs['url']

            uParse = urlparse(__url)

            if uParse.scheme in self.schms:
                self.url = __url
            else:
                print('expected a url to get but invalid url was detected.')
        else:
            for arg in args:
                uParse = urlparse(arg)

                if uParse.scheme in self.schms:
                    self.url = arg

        self.chkList(krgs)

        if self.url is not None:
            if not isinstance(self.url, list):
                cmd = ['youtube-dl', '-f', 'bestaudio[ext=m4a]', self.url]
                self.cmdReadOut(cmd)
            else:
                for mUrl in self.url:
                    print(f'Attempting to download {mUrl}')
                    cmd = ['youtube-dl', '-f', 'bestaudio[ext=m4a]', mUrl]
                    self.cmdReadOut(cmd)

        else:
            print('expected a url to get but invalid url was detected.2')


    def chkList(self, krgs):
        '''  '''

        if self.url is not None and ',' in krgs['url']:
            __url = [x.strip() for x in krgs['url'].split(',')]
            self.url = []
            for ul in __url:
                uParse = urlparse(ul)
                if uParse.scheme in self.schms:
                    self.url.append(ul)
                else:
                    print('expected a url to get but invalid url was detected.')


    def cmdReadOut(self, cmd):
        ''' Read progress in real time to stdout and log output for return. '''
        process = subp.Popen(cmd, bufsize=1, universal_newlines=True, stdout=subp.PIPE, stderr=subp.STDOUT)
        outPut = ''
        for line in iter(process.stdout.readline, ''):
            if line != '':
                outPut = f'{outPut}\n{line}'
                stdout.buffer.write(line.replace('\n', '\r').encode())
            stdout.flush() # please see comments regarding the necessity of this line
        process.wait()
        errcode = process.returncode
        return (outPut, True) if errcode == 0 else (outPut, errcode)


def mkArgs(dlArgs):
    if len(dlArgs) > 1:
        lt, dt = [], {}
        for ar in dlArgs[1:]:
            if '=' in ar:
                a = ar.split('=', 1)
                dt[a[0]] = a[1]
            else:
                lt.append(ar)
        return (lt, dt)
    else:
        return None

if __name__ == '__main__':

    argsv = mkArgs(argv)
    
    if argsv is None:
        sExit('die')
    else:
        youAudio(*argsv[0], **argsv[1])
